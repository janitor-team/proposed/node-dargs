// adapted from readme.md
const dargs = require('dargs')

const input = {
        _: ['some', 'option'],          // values in '_' will be appended to the end of the generated argument list
        foo: 'bar',
        hello: true,                    // results in only the key being used
        cake: false,                    // prepends `no-` before the key
        camelCase: 5,                   // camelCase is slugged to `camel-case`
        multiple: ['value', 'value2'],  // converted to multiple arguments
        pieKind: 'cherry',
        sad: ':('
};

const excludes = ['sad', /.*Kind$/];  // excludes and includes accept regular expressions
const includes = ['camelCase', 'multiple', 'sad', /^pie.*/];
const aliases = {file: 'f'};

console.log(dargs(input, {excludes}));
